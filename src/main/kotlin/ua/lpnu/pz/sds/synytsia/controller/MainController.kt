package ua.lpnu.pz.sds.synytsia.controller

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleLongProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.Controller
import ua.lpnu.pz.sds.synytsia.model.Generator
import ua.lpnu.pz.sds.synytsia.model.Params

class MainController : Controller() {
    private val generator = Generator(BUFFER_SIZE)
    val m = SimpleLongProperty(1_048_575)
    val a = SimpleLongProperty(343)
    val c = SimpleLongProperty(89)
    val seed = SimpleIntegerProperty(1)
    val amount = SimpleIntegerProperty(10)
    val filePath = SimpleStringProperty("""C:\Users\User\Desktop\Output.txt""")

    val generatedValues = SimpleStringProperty()

    fun generate() {
        generatedValues.value = "Started generating..."
        runAsync {
            generator.generate(Params(m.value, a.value, c.value, seed.value), amount.value, filePath.value)
        } ui {
            generatedValues.value = it.joinToString(prefix = "Period: ${generator.period}\n\n")
        }
    }

    companion object {
        const val BUFFER_SIZE = 536_870_912
    }
}