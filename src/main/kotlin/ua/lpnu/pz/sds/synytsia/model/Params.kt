package ua.lpnu.pz.sds.synytsia.model

data class Params(
    /**
     * Modulus
     */
    val m: Long,
    /**
     * Multiplier
     */
    val a: Long,
    /**
     * Increment
     */
    val c: Long,
    /**
     * Seed
     */
    val seed: Int
)