package ua.lpnu.pz.sds.synytsia

import ua.lpnu.pz.sds.synytsia.view.MainView
import tornadofx.App

class Application: App(MainView::class, Styles::class)