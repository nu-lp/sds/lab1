package ua.lpnu.pz.sds.synytsia.view

import javafx.geometry.Pos
import javafx.scene.layout.Priority
import tornadofx.*
import ua.lpnu.pz.sds.synytsia.controller.MainController

class MainView : View("Lab1 - Pseudo-random numbers generator") {
    private val controller: MainController by inject()

    override val root = borderpane {

        left = form {
            fieldset("Generator parameters") {
                field("Modulus (m)") {
                    textfield(controller.m) {
                        filterInput {
                            it.controlNewText.let { v ->
                                v.isLong() && v.toLong() > 0
                            }
                        }
                    }
                }
                field("Multiplier (a)") {
                    textfield(controller.a) {
                        filterInput {
                            it.controlNewText.let { v ->
                                v.isLong() && v.toLong() > 0
                            }
                        }
                    }

                }
                field("Increment (c)") {
                    textfield(controller.c) {
                        filterInput {
                            it.controlNewText.let { v ->
                                v.isLong() && v.toLong() >= 0
                            }
                        }
                    }

                }
                field("Seed (X₀)") {
                    textfield(controller.seed) {
                        filterInput {
                            it.controlNewText.let { v ->
                                v.isInt() && v.toInt() >= 0
                            }
                        }
                    }
                }
            }
            fieldset("Output parameters") {
                field("Amount of values (n)") {
                    textfield(controller.amount) {
                        filterInput {
                            it.controlNewText.let { v ->
                                v.isInt() && v.toInt() > 0
                            }
                        }
                    }
                }
                field("Output filepath") {
                    hbox {
                        label(controller.filePath) {
                            hgrow = Priority.ALWAYS
                            useMaxSize = true
                        }
                        button("Browse...") {
                            hboxConstraints {
                                marginLeft = 16.0
                            }
                            action {
                                val dir = chooseDirectory("Select target directory")
                                dir?.let {
                                    controller.filePath.set("$dir\\$FILENAME")
                                }
                            }
                        }

                    }
                }
            }
            hbox {
                alignment = Pos.CENTER
                button("Generate") {
                    action {
                        controller.generate()
                    }
                }
            }
        }
        right = scrollpane {
            prefWidth = 600.0
            text {
                wrappingWidth = 600.0
                textProperty().bind(controller.generatedValues)
            }
        }
    }

    companion object {
        const val FILENAME = "Output.txt"
    }
}