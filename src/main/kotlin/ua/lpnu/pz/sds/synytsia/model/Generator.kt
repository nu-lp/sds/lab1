package ua.lpnu.pz.sds.synytsia.model

import java.io.BufferedWriter
import java.io.FileWriter
import java.io.PrintWriter
import java.util.*

class Generator(private val bufferSize: Int) {

    private lateinit var p: Params
    private val periodCheck = BitSet(Int.MAX_VALUE)
    private var generated = -1
    private var periodCounter = 0
    var period = -1
        private set

    fun generate(params: Params, amount: Int, pathToFile: String): IntArray {
        validate(params)
        init(params)

        val result = IntArray(amount)
        result[0] = params.seed
        updatePeriod()


        PrintWriter(BufferedWriter(FileWriter(pathToFile), bufferSize)).use {
            it.print(params.seed)
            for (i in 1 until amount) {
                result[i] = generateNext()
                if (period == -1) {
                    it.print(", ${result[i]}")
                }
            }
            // May still not reach the period
            while (period == -1) {
                it.print(", ${generateNext()}")
            }
        }

        return result
    }

    private fun validate(params: Params) {
        val (m, a, c, seed) = params
        if (m <= 0) {
            throw IllegalArgumentException("Modulus has to be more than zero. Actual: $m")
        }
        if (a <= 0 || a >= m) {
            throw IllegalArgumentException("Multiplier has to be more than zero and less than modulus. Actual: $a")
        }
        if (c < 0 || c >= m) {
            throw IllegalArgumentException("Increment has to be more or equal zero and less than modulus. Actual: $c")
        }
        if (seed < 0 || seed >= m) {
            throw IllegalArgumentException("Seed has to be more or equal zero and less than modulus. Actual: $seed")
        }
    }

    private fun init(params: Params) {
        periodCheck.clear()
        p = params
        generated = params.seed
        periodCounter = 0
        period = -1
    }

    private fun generateNext(): Int {
        generated = ((p.a * generated + p.c) % p.m).toInt()
        updatePeriod()
        return generated
    }

    private fun updatePeriod() {
        if (period == -1) {
            if (periodCheck[generated]) {
                period = periodCounter
            }
            periodCounter++
            periodCheck.set(generated)
        }
    }
}